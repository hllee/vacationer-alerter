package com.mantech.vacationalert

import org.apache.camel.CamelContext
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
class AlertRouter(context: CamelContext?) : RouteBuilder(context) {
    @Throws(Exception::class)
    override fun configure() {
        from("scheduler://foo?scheduler=spring&scheduler.cron=0 * * * * *").bean("findVacationerService", "getVacationers")
                .to("log:bar")
    }
}
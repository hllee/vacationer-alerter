package com.mantech.vacationalert

import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Service
class VacationTimeService {

    var holidays = setOf<String>("20190501","20190506")
    var dateTimeFormater: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd")

    fun getNextWorkingDay(curDay: LocalDate): LocalDate {
        var nextDay = curDay.plusDays(1);
        if (isWorkDay(nextDay)) {
            return nextDay
        }
        return getNextWorkingDay(nextDay)
    }

    fun isWorkDay(curDay: LocalDate): Boolean {
        if (curDay.dayOfWeek.value > 5) {
            return false;
        }
        return !isHoliday(curDay);
    }


    fun isHoliday(curDay: LocalDate): Boolean {
        return holidays.contains(curDay.format(dateTimeFormater))
    }
}
package com.mantech.vacationalert

enum class VacationType(val code: String) {
    AllDay("d"),
    Morning("m"),
    AFTERNOON("a")
}

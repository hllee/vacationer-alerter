package com.mantech.vacationalert

class VacationInfo(var vacationer: String, var name: String, var vacationMin: Long, var vacationMax: Long, var vacationType: VacationType)
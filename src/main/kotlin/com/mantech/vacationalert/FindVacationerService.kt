package com.mantech.vacationalert

import org.springframework.stereotype.Service
import java.lang.RuntimeException
import java.time.LocalDate

@Service
class FindVacationerService(var vacationTimeService: VacationTimeService) {

    fun getVacationers():List<VacationInfo> {
        var today = LocalDate.now()
        if(!vacationTimeService.isWorkDay(today)) {
            throw RuntimeException("it is holiday")
        }
        println("just pring vacationers")
        return listOf<VacationInfo>(VacationInfo("hllee", 30000, 70000, VacationType.AllDay))
    }

    
}
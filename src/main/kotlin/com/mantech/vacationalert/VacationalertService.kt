package com.mantech.vacationalert

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class VacationalertApplication

fun main(args: Array<String>) {
	runApplication<VacationalertApplication>(*args)
}
